<?php

$bdd = new PDO("mysql:host=127.0.0.1;dbname=user-registration;charset=utf8", "root", "");
if(isset($_POST['article_titre'], $_POST['article_contenu'])) {
   if(!empty($_POST['article_titre']) AND !empty($_POST['article_contenu'])) {
      
      $article_titre = htmlspecialchars($_POST['article_titre']);
      $article_contenu = htmlspecialchars($_POST['article_contenu']);
      $ins = $bdd->prepare('INSERT INTO articles (title, content, date_publication) VALUES (?, ?, NOW())');
      $ins->execute(array($article_titre, $article_contenu));
      $message = 'Votre article a bien été posté';
   } else {
      $message = 'Veuillez remplir tous les champs';
   }
}

?>

<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta charset="utf-8" />
        <link href="../assets/css/miniblog-style.css" type="text/css"
		rel="stylesheet" />
        <link href="../assets/css/user-registration.css" type="text/css"
		rel="stylesheet" />
      <title>Rédaction</title>
</head>
<body>
   <div class='article-container'>
      <form method="POST">
      <h1 class="articleh1">Entrez le contenu de votre article ici :</h1>
      <br>
         <input class='champstitre' type="text" name="article_titre" placeholder="Titre" /><br />
         <hr>
         <hr>
         <textarea class='champstexte' name="article_contenu" placeholder="Contenu de l'article"></textarea><br />
         <hr>
         <input type="submit" value="Envoyer l'article" />
         <hr>
         <a href="../home.php">Retour aux articles</a>
      </form>
      <br />
      <?php if(isset($message)) { echo $message; } ?>
   </div>
</body>
</html>