<?php
if(!isset($_GET['id']) OR !is_numeric($_GET['id'])){}
    //header('Location: /../home.php');
else{

    extract($_GET);
    $id = strip_tags($id);

    require_once('function.php');
    $articles = getArticles($id);
}


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link href="../assets/css/miniblog-style.css" type="text/css"
		rel="stylesheet" />
        <link href="../assets/css/user-registration.css" type="text/css"
		rel="stylesheet" />
        <TITLE><?= $articles->title ?></TITLE>
    </head>
    <body>
        <div class="article-read">
            <h1><?= $articles->title ?></h1>
            <hr>
            <hr>
            <p><?= $articles->content ?></p>
            <hr>
            <a href="../home.php">Retour aux articles</a>
        </div>
    </body>
</html>