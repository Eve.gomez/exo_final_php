<?php
session_start();
if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
    session_write_close();
} else {
    // puisque le nom d’utilisateur n’est pas défini en session, l’utilisateur n’est pas connecté
    // il essaie d’accéder à cette page non autorisée
    // donc nous allons effacer toutes les variables de session et le rediriger vers l’index
    session_unset();
    session_write_close();
    $url = "./index.php";
    header("Location: $url");
}

require_once('articles/function.php');
$articles = getArticle();

?>

<!-- Html correspondant a l'utilisateur identifié -->
<HTML>
<HEAD>
<TITLE>Welcome</TITLE>
<link href="assets/css/miniblog-style.css" type="text/css"
	rel="stylesheet" />
<link href="assets/css/user-registration.css" type="text/css"
	rel="stylesheet" />
</HEAD>
<BODY>
	<div class="miniblog-container">
		<div class="page-header">
			<span class="login-signup"><a href="logout.php">Logout</a></span>
		</div>
		<div class="page-content">Welcome <?php echo $username;?></div>
	</div>
    <div class='articlechoix'>
        <div class='articles-container'>
            <h1>Articles: </h1>
            <hr>
            <h2>Pour créer ton article,</h2>
            <a href="articles/creation.php">C'est par ici</a><br><br>
            <hr>
        </div>
    </div>
    <div class='articlechoix'>
        <div class='articles-container'>
            <h3>Articles Publiés</h3>
            <hr>
            <?php foreach($articles as $articles): ?>   
                <h2><?= $articles->title ?></h2>
            <a href="articles/article.php?id=<?= $articles->id ?>"> Lire la suite</a>
            <br><br>
            <hr>
            <?php endforeach; ?> 
            <br>
        </div>
    </div>
</BODY>
</HTML>
