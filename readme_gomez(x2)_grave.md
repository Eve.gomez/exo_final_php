# exo_final_php

Le dossier complet de l'exo final est disponible sur la branche develop, chaque autre branches sont des étapes plus ou moins abouties du blog.
Vous retrouverez également sur la branhce develop l'ennoncé avec les étapes complétées. 

# Éxercice

Votre objectif est de réaliser un mini-site de blog. 
Un utilisateur pourra : 

- s'enregistrer
- se connecter
- ajouter un post (en étant connecté)
- visualiser sa liste de post (en étant connecté)
- se déconnecter

## Consignes : 

- vous enverrez par mail un lien github/gitlab public, contenant l'ensemble de vos fichiers
- Le présent fichier sera à la racine de votre git, nommé README.md. Vous y aurez cocher les cases des fonctionnalités réalisées, en indiquant à côté de la case à cocher le prénom de la personne qui à réalisé l'étape. 

## Groupe : 

Groupe: 
GOMEZ Eve
GOMEZ léo
GRAVE Léa

## Étapes : 

### Enregistrement : 

 - [X] **léo** Front
   - [X] **léo** formulaire d'enregistrement :
     - [X] **léo** email
     - [X] **léo** pseudo
     - [X] **léo** mot de passe
     - [X] **léo** confirmation du mot de passe
 - [X] **léo** back :
   - [ ] **léo** validation et nettoyage des données saisies
   - [X] **léo** Connexion à la base de données
     - [X] **léo** vérification que l'email et le pseudo ne sont pas déjà utilisés
   - [X] **léo** vérification que les mots de passes saisis sont identiques (mot de passe et mot de passe de confirmation)
   - [X] **léo** Si tout OK, 
     - [X] **léo** hash du mot de passe
     - [X] **léo** enregistrement de l'utilisateur dans la base, avec mot de passe hashé
     - [X] **léo** création de session
     - [X] **léo** redirection sur la page de saisie de post
   - [X] **léo** Si erreur, affichage de message d'erreur, et d'un lien pour revenir à l'enregistrement

### Login 

 - [X] **léa** Front
   - [X] **léa** formulaire de connexion :
     - [X] **léa** un champ email
     - [X] **léa** un champ mot de passe
 - [X] **léa** Back 
   - [X] **léa** Validation et récupération des données
   - [X] **léa** Recherche de l'utilisateur dans la base de donnée
     - [X] **léa** Si trouvé, vérification du mot de passe
       - [X] **léa** Si ok, création de la session, et redirection sur la page des posts
   - [X] **léa** Si erreur, affichage de message d'erreur, et d'un lien pour revenir à la connexion

## Affichage des posts : 

 - [X] **léa** Vérification de l'état de connexion de l'utilisateur 
   - [X] **léa** Si connecté, 
     - [ ] **léa** recherche de tous les posts de l'utilisateur en base de données
     - [X] **léa** parcours et affichage des posts
     - [X] **léa** affichage de liens
       - [X] **léa** vers la saisie d'un post
       - [X] **léa** pour se déconnecter
   - [X] **léa** si non connecté : redirection vers la page de connexion

## Saisie d'un post

 - [X] **Eve** Front (PHP + html): 
   - [X] **Eve** si utilisateur connecté
     - [X] **Eve** formulaire de saisie du post + bouton d'envoi
     - [X] **Eve** lien de retour à l'affichage des posts de l'utilisateur
   - [X] **Eve** Si non connecté : redirection vers la page de connexion
 - [X] **Eve** Back : 
   - [X] **Eve** Validation et récupération des données
   - [X] **Eve** Connexion à la BDD
   - [X] **Eve** Préparation de la requête d'insertion du post
   - [X] **Eve** association des paramètres
   - [X] **Eve** execution de la requête
   - [X] **Eve** Si tout s'est bien passé, retour à la liste des posts
