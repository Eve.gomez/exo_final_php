<?php
use miniblog\Member;

// On fais correspondre le fomulaire html avec la base de donnée via member.php 
if (! empty($_POST["login-btn"])) {
    require_once __DIR__ . '/Model/Member.php';
    $member = new Member();
    $loginResult = $member->loginMember();
}
?>
<HTML>
<HEAD>
	<!-- Formulaire de Login  -->
	<TITLE>Login</TITLE>

	<!-- Styles -->
	<link href="assets/css/miniblog-style.css" type="text/css"
		rel="stylesheet" />
		
	<link href="assets/css/user-registration.css" type="text/css"
		rel="stylesheet" />
</HEAD>

<BODY>
	<div class="miniblog-container">
		<div class="sign-up-container">

			<!-- Bouton pour passer sur l'enregistrement de l'utilisateur  -->
			<div class="login-signup">
				<a href="user-registration.php">Sign up</a>
			</div>

			<!-- Formulaire de LOGIN -->
			<div class="signup-align">
				<form name="login" action="" method="post"
					onsubmit="return loginValidation()">
					<div class="signup-heading">Login</div>

				<?php if(!empty($loginResult)){?>

				<div class="error-msg"><?php echo $loginResult;?></div>
				<?php }?>

				<div class="row">
						<div class="inline-block">
							<!-- username  -->
							<div class="form-label">
								Username<span class="required error" id="username-info"></span>
							</div>
							<input class="input-box-330" type="text" name="username"
								id="username">
						</div>
					</div>
					<div class="row">
						<div class="inline-block">
							<!-- label  -->
							<div class="form-label">
								Password<span class="required error" id="login-password-info"></span>
							</div>
							<input class="input-box-330" type="password"
								name="login-password" id="login-password">
						</div>
					</div>
					<!-- envoyer  -->
					<div class="row">
						<input class="btn" type="submit" name="login-btn"
							id="login-btn" value="Login">
					</div>
				</form>
			</div>
		</div>
	</div>

	<script>
	// Validation des elements rentrés dans le formulaire
	function loginValidation() {
		var valid = true;
		$("#username").removeClass("error-field");
		$("#password").removeClass("error-field");

		var UserName = $("#username").val();
		var Password = $('#login-password').val();

		$("#username-info").html("").hide();

		if (UserName.trim() == "") {
			$("#username-info").html("required.").css("color", "#ee0000").show();
			$("#username").addClass("error-field");
			valid = false;
		}
		if (Password.trim() == "") {
			$("#login-password-info").html("required.").css("color", "#ee0000").show();
			$("#login-password").addClass("error-field");
			valid = false;
		}
		if (valid == false) {
			$('.error-field').first().focus();
			valid = false;
		}
		return valid;
	}
</script>
</BODY>
</HTML>
